/*
 * This class creates a new stack (T) to be used
 * in the creation and calling of the following.
 * @author ddietz@email.sc.edu
 */
import java.util.LinkedList;

public class Stack<T> {
    private LinkedList<T> list = new LinkedList<T>();

    public void push(T e) {
        list.addFirst(e);
    } // end push method

    public T pop() {
        return list.removeFirst();
    } // end pop method

    public T peek() {
        return list.getFirst();
    } // end peek method 

    public int size() {
        return list.size();
    } // end size method
    
} // end class Stack
