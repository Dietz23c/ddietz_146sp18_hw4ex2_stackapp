/*
This class creates a new stack that is called with
the following created methods within an enhanced for
loop and while loop. Then is concatinated with each method 
then called by the Stack package. After being called, the 
output will be the concatination with a sentence that is a string
that identifies the data being presented.

@ddietz@email.sc.edu

*/
public class StackApp {

    public static void main(String[] args) {
        
    Stack<String> e = new Stack<>();
    
    String[] stackArray = new String[]{"Apples","Oranges","Bananas"};
    
    for(String i: stackArray){
        e.push(i);
        System.out.println("Push: " + i);
       
    } // end for loop
    
    System.out.println("The stack contains " + e.size() + " items" + "\n");
    
     
    System.out.println("Peek: " + e.peek());
    System.out.println("The stack contains " + e.size() + " items" + "\n");
    
    while(e.size() > 0 ){
 
        System.out.println("Pop: " + e.pop());
        
    }// end while loop 
    
    System.out.println("The stack contains " + e.size() + " items");
    
    } // end main method
    
} // end StackApp class 
